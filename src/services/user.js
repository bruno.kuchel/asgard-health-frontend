import { clientHttp } from '../config/config.js'

const CreateUser = (data) => clientHttp.post(`/user`, data)

const ListUser = () => clientHttp.get(`/user`)

const DeleteUser = (email) => clientHttp.delete(`/user/${email}`)

const UpdateUser = (data) => clientHttp.patch(`/user/${data._id}`, data)

const ShowUser = (id) => clientHttp.get(`/user/${id}`) 


export {
    CreateUser,
    ListUser,
    DeleteUser,
    UpdateUser,
    ShowUser
}
