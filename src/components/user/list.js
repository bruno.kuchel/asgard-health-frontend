import React, { useEffect, useState } from 'react'
import { ListUser } from '../../services/user'
import { useHistory } from 'react-router-dom'
import Loading from '../loading/loading'
import Confirmation from '../alert/confirmation/user'
import './user.css'
import jwt from 'jsonwebtoken'
import { getToken } from '../../config/auth'


const UserList = (props) => {
    const [users, setUsers] = useState([])
    const [loading, setLoading] = useState(false)
    const [confirmation, setConfirmation] = useState({
        isShow: false,
        params: {}
    })
    const [userIsAdmin, setUserIsAdmin] = useState({})


    const getList = async () => {
        try {
            setLoading(true)
            const users = await ListUser()
            if (users) {
                setUsers(users.data)
            }
            setLoading(false)
        } catch (error) {
            setLoading(false)
        }
    }

    const history = useHistory()

    const editUser = (user) => props.history.push(`/editar-paciente/${user._id}`)

    const deleteUser = (user) => setConfirmation({
        isShow: true,
        params: user
    })

   
    
  
    useEffect(() => {
      (async () => {
          const { user } = await jwt.decode(getToken())
          setUserIsAdmin(user.is_admin)
      })()
      return () => { }
  }, [])

    useEffect(function () {
        getList()
    }, [confirmation])

    

    return (
        <section className="sectionList">
            <div className="title">
                <h1>Pacientes Asgard Health</h1>
            </div>           

                {confirmation.isShow ? (
                    <Confirmation data={confirmation} fnc={setConfirmation} />
                ) : (
                    <div className="listaPacientes">
                <nav>
                    <div className="action">
                        <button className="btnNavList" name="adicionar" onClick={() => history.push('/')}><i className="fa fa-th-list iconsNav"></i>MENU</button>
                        <button className="btnNavList" name="adicionar" onClick={() => history.push('/nuevo-paciente')}><i className="fa fa-user-plus iconsNav"></i>NUEVO</button>
                    </div>
                </nav>
                        <section>
                            <div className="list_user">
                                {loading ? <Loading/>
                                :<div>                 
                                    { users.map((user, index) => (
                                    <div key={index} className="row-list">
                                        <div className="user-header">
                                            <div className="user-name">{user.nome} {user.sobrenome}</div>
                                            <div className="user-action">
                                                <button className="btnAction" disabled={userIsAdmin ? false : true} onClick={() => editUser(user)}>                                                    
                                                    <i className="fa fa-edit"></i>
                                                </button>
                                                <button className="btnAction" disabled={userIsAdmin ? false : true} onClick={() => deleteUser(user)}>                                                    
                                                    <i className="fa fa-trash"></i>
                                                </button>                                            
                                                
                                            </div>
                                        </div>
                                        <div className="row-data">
                                            <div className="user-data"><span>Sexo</span> {user.sexo}</div>
                                            <div className="user-data"><span>Nacimiento</span> {user.nascimento}</div>
                                            <div className="user-data"><span>DNI</span> {user.rg}</div>
                                            <div className="user-data"><span>Teléfono</span> {user.telefone}</div>
                                            <div className="user-data"><span>E-mail</span> {user.email}</div>                    
                                        </div>                        
                                    </div>
                                ))
                            
                            } </div>}
                            

                            </div>
                        </section>
                        </div>
                    )}
            
        </section>
    )
}

export default UserList
