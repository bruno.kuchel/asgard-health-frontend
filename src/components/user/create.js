import React, { useState, useEffect } from 'react'
import { CreateUser, ShowUser, UpdateUser } from '../../services/user'
import { useHistory, useParams } from 'react-router-dom'
import Alert from '../alert/index'
import './user.css'

const UserCreate = (props) => {
    const [isEdit, setisEdit] = useState(false)
    const { _id } = useParams()
    const history = useHistory()
    const [alert, setAlert] = useState({})
    const method = isEdit ? UpdateUser : CreateUser
    const [isSubmit, setIsSubmit] = useState (false)

    const [form, setForm] = useState({
        is_active: true,
        is_admin: false

    })

    useEffect(() => {
        const getShowUser = async () => {
            const user = await ShowUser(_id)
            setForm(user.data)            
        }
        if (_id) {
            setisEdit(true)
            getShowUser()     
                  
        }
    }, [_id]

    )

    const handleChange = (event) => {
       
        setForm({
            ...form,
            [event.target.name]: event.target.value
        })
        return
    }

    const formIsValid = () => {
        let radioButtonIsValid = false
         let radiosButtons = document.getElementsByName('sexo')
 
         radiosButtons.forEach((radioButton) => {
             if (radioButton.checked) {
                 radioButtonIsValid = true
             }
        })
        return (
            radioButtonIsValid &&            
            form.nascimento &&
            form.nome &&
            form.sobrenome &&
            form.rg &&
            form.email &&
            form.telefone
        )
    }

    const checkMasculino = () => {
        if(form.sexo === "Hombre"){
            document.getElementById("masculino").checked = true
        }      
    }

    const checkFeminino = () => {
        if(form.sexo === "Mujer"){
            document.getElementById("feminino").checked = true
        }
    }
   

    const submitForm = async (event) => {
        try {
            setIsSubmit(true)
            await method(form)
            
            setForm({
                is_active: true,
                is_admin: false
            })

            setAlert({
                type: "success",
                message: "Formulario enviado con éxito",
                show: true
            })

            setTimeout(() => 
                history.push('/pacientes')
            , 2000);

            
        } catch (error) {
            setAlert({
                type: "error",
                message: "Ha habido un error...",
                show: true
            })
            setIsSubmit(false)

        }
    }

    
    return (
        <section>
            <div className="title">
                <h1>{isEdit ? 'Editar paciente' : 'Nuevo paciente'}</h1>
            </div>
            
            <div className="cadastroPaciente">
            <Alert type={alert.type || ""} message={alert.message || ""} show={alert.show || false} />                
                <div className="row">
                    <div className="radios">
                        <div className="etiqueta">Sexo</div>
                        <div className="radio">
                            <input disabled={isSubmit} className="option" type="radio" id="masculino" name="sexo" onChange={handleChange} value="Hombre" checked={isEdit ? checkMasculino() : form.masculino}/>
                            <label htmlFor="masculino">Hombre</label>
                        </div>
                        <div className="radio">
                            <input disabled={isSubmit} className="option" type="radio" id="feminino" name="sexo" onChange={handleChange} value="Mujer" checked={isEdit ? checkFeminino() : form.feminino}/>
                            <label htmlFor="feminino">Mujer</label>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="etiqueta">
                        <label htmlFor="nascimento">Fecha de nacimiento:</label>
                    </div>
                    <div className="form-input">
                        <input disabled={isSubmit} className="campo" type="date" name="nascimento" onChange={handleChange} value={form.nascimento || ""} placeholder="Fecha de nacimiento" />
                    </div>
                </div>
                <div className="row">
                    <div className="etiqueta">
                        <label htmlFor="nome">Nombre:</label>
                    </div>
                    <div className="form-input">
                        <input disabled={isSubmit} className="campo" type="text" name="nome" label="Nombre:" onChange={handleChange} value={form.nome || ""} placeholder="Indica el nombre del paciente" autoComplete="false" maxLength="20"/>
                    </div>
                </div>
                <div className="row">
                    <div className="etiqueta">
                        <label htmlFor="sobrenome">Apellido:</label>
                    </div>
                    <div className="form-input">
                        <input disabled={isSubmit} className="campo" type="text" name="sobrenome" onChange={handleChange} value={form.sobrenome || ""} placeholder="Indica el apellido del paciente" autoComplete="false" maxLength="20"/>
                    </div>
                </div>
                <div className="row">
                    <div className="etiqueta">
                        <label htmlFor="rg">DNI:</label>
                    </div>
                    <div className="form-input">
                        <input disabled={isSubmit} className="campo" type="text" min="0" name="rg" onChange={handleChange} value={form.rg || ""} placeholder="Indica el DNI del paciente" autoComplete="false" maxLength="9"/>
                    </div>
                </div>
                <div className="row">
                    <div className="etiqueta">
                        <label htmlFor="email">E-mail:</label>
                    </div>
                    <div className="form-input">
                        <input disabled={isSubmit} className="campo" type="email" name="email" onChange={handleChange} value={form.email || ""} placeholder="Indica el e-mail del paciente" autoComplete="false"/>
                    </div>
                </div>
                <div className="row">
                    <div className="etiqueta">
                        <label htmlFor="telefone">Teléfono:</label>
                    </div>
                    <div className="form-input">
                        <input disabled={isSubmit} className="campo" type="tel" name="telefone" onChange={handleChange} value={form.telefone || ""} placeholder="Indica el teléfono del paciente" autoComplete="false" maxLength="9"/>
                    </div>
                </div>
                <div className="row">
                    <button disabled={!formIsValid()} className="btn cadastrar" onClick={submitForm}>{isEdit ? 'EDITAR' : 'ENVIAR'}</button>
                </div>
                <div className="row">
                    <button className="btn cadastrar" onClick={() => history.goBack()}>CANCELAR</button>
                </div>
            </div>
        </section>
    )
}
export default UserCreate
