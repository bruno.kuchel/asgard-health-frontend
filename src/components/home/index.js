import React from 'react'
import { useHistory } from 'react-router-dom' 
import './home.css'

const Home = (props) => { 

  const history = useHistory()
  
  return (
    <section className="home">
      <div className="title">
        <h1>Menú principal</h1>
      </div>
      <div className="nav">
        <button className="homeBtn" onClick={() =>history.push('/pacientes')}><i className="fa fa-users i"></i>PACIENTES</button>
        <button className="homeBtn" onClick={() => history.push('/nuevo-paciente')}><i className="fa fa-user-plus i"></i>NUEVO PACIENTE</button>        
          <button className="homeBtn" onClick={() => history.push('/empleados')}><i className="fa fa-user-md i"></i>EMPLEADOS</button>       
      </div>
    </section>
  )

}

export default Home